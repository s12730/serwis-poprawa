create table ADVERTS (
	ID INTEGER IDENTITY PRIMARY KEY,
	TITLE varchar(50),
	DESCRIPTION varchar(50),
	PRICE DOUBLE,
	SIZE INTEGER,
	ADDRESS varchar(50),
	PHONE varchar(50),
	EMAIL varchar(50),
	TYPE varchar(50)
);