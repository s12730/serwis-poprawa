package com.adverts.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.adverts.entity.AdvertEntity;
import com.adverts.service.AdvertService;


@Controller
@RequestMapping("/adverts")
public class AdvertsController {

 
    @Autowired
    private AdvertService advertService;
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Map<String, Object> map) {
 
        map.put("advert", new AdvertEntity());
        map.put("advertList", advertService.getAll());
 
        return "advert";
    }
 
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String add(@ModelAttribute("advert") AdvertEntity advert, BindingResult result) {
    	advertService.create(advert);
        return "redirect:/adverts/list";
    }
 
    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable("id")
    Integer id) {
    	advertService.delete(id);
        return "redirect:/adverts/list";
    }
}