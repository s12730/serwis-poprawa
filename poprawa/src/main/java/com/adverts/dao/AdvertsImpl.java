package com.adverts.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.adverts.entity.AdvertEntity;

@Repository
public class AdvertsImpl implements AdvertsDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void create(AdvertEntity order) {
		sessionFactory.getCurrentSession().save(order);

	}

	public List<AdvertEntity> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from AdvertEntity")
				.list();
	}

	public void delete(Integer id) {
		AdvertEntity advert = (AdvertEntity) sessionFactory.getCurrentSession()
				.load(AdvertEntity.class, id);
		if (null != advert) {
			sessionFactory.getCurrentSession().delete(advert);
		}

	}

}
