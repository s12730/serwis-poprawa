package com.adverts.dao;

import java.util.List;

import com.adverts.entity.AdvertEntity;

public interface AdvertsDao {

	public void create(AdvertEntity advert);

	public List<AdvertEntity> getAll();

	public void delete(Integer id);

}
