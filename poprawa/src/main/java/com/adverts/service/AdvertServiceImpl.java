package com.adverts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adverts.dao.AdvertsDao;
import com.adverts.entity.AdvertEntity;

@Service
public class AdvertServiceImpl implements AdvertService {

	@Autowired
	private AdvertsDao advertDAO;

	@Transactional
	public void create(AdvertEntity entity) {
		advertDAO.create(entity);
	}

	@Transactional
	public List<AdvertEntity> getAll() {
		return advertDAO.getAll();
	}

	@Transactional
	public void delete(Integer id) {
		advertDAO.delete(id);
	}

}
