package com.adverts.service;

import java.util.List;

import com.adverts.entity.AdvertEntity;

public interface AdvertService {

	public void create(AdvertEntity entity);

	public List<AdvertEntity> getAll();

	public void delete(Integer id);

}
