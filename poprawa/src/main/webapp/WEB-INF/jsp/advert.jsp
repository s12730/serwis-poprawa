<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Adverts</title>
</head>
<body>

<form:form method="post" action="save.html" commandName="advert">

<h3>New advertisment:</h3>
	<table>
	<tr>
		<td><form:label path="Title">Title</form:label></td>
		<td><form:input path="Title" /></td> 
	</tr>
	<tr>
		<td><form:label path="Description">Description</form:label></td>
		<td><form:input path="Description" /></td>
	</tr>
	<tr>
		<td><form:label path="Price">Price</form:label></td>
		<td><form:input path="Price" /></td>
	</tr>
	<tr>
		<td><form:label path="Size">Size</form:label></td>
		<td><form:input path="Size" /></td>
	</tr>
	<tr>
		<td><form:label path="Address">Address</form:label></td>
		<td><form:input path="Address" /></td>
	</tr>
	<tr>
		<td><form:label path="Phone">Phone</form:label></td>
		<td><form:input path="Phone" /></td>
	</tr>
	<tr>
		<td><form:label path="Email">Email</form:label></td>
		<td><form:input path="Email" /></td>
	</tr>
		<tr>
		<td><form:label path="Type">Type</form:label></td>
		<td><form:input path="Type" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="Add"/>
		</td>
	</tr>
</table>	
</form:form>

	
<h3>Adverts:</h3>
<c:if  test="${!empty advertList}">
<table>

	<th>Title</td>
	<th>Description</td>
	<th>Price</td>
	<th>Size</td>
	<th>Address</td>
	<th>Phone</td>
	<th>Email</td>
	<th>Type</td>

<c:forEach items="${advertList}" var="advert">
	<tr>
		<td>${advert.title}</td>
		<td>${advert.description}</td>
		<td>${advert.price}</td>
		<td>${advert.size}</td>
		<td>${advert.address}</td>
		<td>${advert.phone}</td>
		<td>${advert.email}</td>
		<td>${advert.type}</td>
		<td><a href="delete/${advert.id}">delete</a></td>
	</tr>
</c:forEach>
</table>
</c:if>


</body>
</html>
